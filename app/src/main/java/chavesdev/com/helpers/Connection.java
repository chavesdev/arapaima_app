package chavesdev.com.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import chavesdev.com.arapaima.R;

/**
 * Created by rodrigo-santos on 09/04/17.
 */

public class Connection {

    private Context context;
    private SharedPreferences sharedPref;
    private String urlInit;


    public Connection(Context context){
        this.context = context;
    }

    public boolean hasUrl(){
        sharedPref = this.context.getSharedPreferences(this.context.getResources().getString(R.string.preference_name), Context.MODE_PRIVATE);
        urlInit = sharedPref.getString(this.context.getResources().getString(R.string.preference_name),null);

        if(urlInit==null) {
            Toast.makeText(this.context, "Você precisa informar o ip do servidor", Toast.LENGTH_LONG).show();
            return false;
        }
        else{
            return true;
        }

    }

    public String getUrlInit() {
        return urlInit;
    }
}
