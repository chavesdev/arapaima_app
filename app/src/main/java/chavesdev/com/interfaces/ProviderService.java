package chavesdev.com.interfaces;

import chavesdev.com.classes.Provider;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rodrigo-santos on 25/03/17.
 */

public interface ProviderService {

    @GET("provider")
    Call<Provider> list();
}
