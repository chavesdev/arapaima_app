package chavesdev.com.interfaces;

import chavesdev.com.classes.Movimentation;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rodrigo-santos on 10/04/17.
 */

public interface MovimentationService {

    @FormUrlEncoded
    @POST("movimentation/add")
    Call<Movimentation> add(
            @Field("qr_code") String qr_code,
            @Field("product_id") int product_id,
            @Field("provider_id") int provider_id,
            @Field("movimentation_type_id") int movimentationType,
            @Field("qtd") int qtd,
            @Field("total") int total,
            @Field("product_status_id") int productStatus,
            @Field("recipient_id") int recipient,
            @Field("aquarium_id") int aquariumId,
            @Field("area_id") int area,
            @Field("created") String created
    );
}
