package chavesdev.com.interfaces;

import chavesdev.com.classes.ProductStatus;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rodrigo-santos on 26/03/17.
 */

public interface ProductStatusService {

    @GET("product_status")
    Call<ProductStatus> list();
}
