package chavesdev.com.interfaces;

import chavesdev.com.classes.MovimentationType;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rodrigo-santos on 26/03/17.
 */

public interface MovimentationTypeService {

    @GET("movimentation_types")
    Call<MovimentationType> list();
}
