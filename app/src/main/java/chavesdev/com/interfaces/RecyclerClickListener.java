package chavesdev.com.interfaces;

import android.view.View;

/**
 * Created by rodrigosantos on 13/04/17.
 */

public interface RecyclerClickListener {

    /**
     * Interface for Recycler View Click listener
     **/

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
