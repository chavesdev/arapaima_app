package chavesdev.com.interfaces;

import java.util.List;

import chavesdev.com.classes.Product;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rodrigo-santos on 24/03/17.
 */

public interface ProductService {

    @GET("product")
    Call<Product> list();
}
