package chavesdev.com.arapaima;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.orm.SugarRecord;
import java.util.List;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.Product;
import chavesdev.com.classes.Saldo;
import chavesdev.com.interfaces.RecyclerClickListener;
import chavesdev.com.listeners.RecyclerTouchListener;
import chavesdev.com.listeners.Toolbar_ActionMode_Callback;
import chavesdev.com.recyclerAdapters.MovimentationListAdapter;
import chavesdev.com.uicontrollers.DividerItemDecoration;

public class MovimentationsActivity extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView recyclerViewMovimentations;
    RecyclerView.LayoutManager mLayoutManager;
    MovimentationListAdapter movimentationListAdapter;
    List<Movimentation> movimentationList = null;

    private ActionMode mActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentations);

        toolbar = (Toolbar) findViewById(R.id.toolbar_movimentations);
        toolbar.setTitle(getResources().getString(R.string.title_activity_movimentations));
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        recyclerViewMovimentations = (RecyclerView) findViewById(R.id.reyclerMovimentations);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        recyclerViewMovimentations.addItemDecoration(itemDecoration);

        recyclerViewMovimentations.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMovimentations.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerViewMovimentations.setLayoutManager(mLayoutManager);

        movimentationListAdapter = new MovimentationListAdapter(movimentationList,this);
        recyclerViewMovimentations.setAdapter(movimentationListAdapter);

        loadMovimentations();
        implementRecyclerViewClickListeners();
    }

    public void loadMovimentations(){
        movimentationList = SugarRecord.listAll(Movimentation.class);
        movimentationListAdapter.setMovimentationList(movimentationList);
        movimentationListAdapter.notifyDataSetChanged();
    }



    //Implement item click and long click over recycler view
    private void implementRecyclerViewClickListeners() {
        recyclerViewMovimentations.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerViewMovimentations, new RecyclerClickListener() {
            @Override
            public void onClick(View view, int position) {
                //If ActionMode not null select item
                if (mActionMode != null)
                    onListItemSelect(position);
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click
                onListItemSelect(position);
            }
        }));
    }

    //List item select method
    private void onListItemSelect(int position) {
        movimentationListAdapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = movimentationListAdapter.getSelectedCount() > 0;//Check if any items are already selected or not


        if (hasCheckedItems && mActionMode == null)
            // there are some selected items, start the actionMode
            mActionMode = ((AppCompatActivity) this).startSupportActionMode((ActionMode.Callback) new Toolbar_ActionMode_Callback(this, movimentationListAdapter, movimentationList, false));
        else if (!hasCheckedItems && mActionMode != null){
            // there no selected items, finish the actionMode
            mActionMode.finish();
            mActionMode = null;
        }

        if (mActionMode != null)
            //set action mode title on item selection
            mActionMode.setTitle(String.valueOf(movimentationListAdapter
                    .getSelectedCount()) +" "+ getResources().getString(R.string.selected));


    }
    //Set action mode null after use
    public void setNullToActionMode() {
        if (mActionMode != null)
            mActionMode = null;
    }

    //Delete selected rows
    public void deleteRows() {
        SparseBooleanArray selected = movimentationListAdapter.getSelectedIds();//Get selected ids

        //Loop all selected ids
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                //If current id is selected remove the item via key
                Movimentation movimentation = movimentationList.get(selected.keyAt(i));
                String aquario = movimentation.getAquariumString();
                Product product = movimentation.findProduct();
                List<Saldo> saldos = Saldo.find(Saldo.class,"aquario = ? and especie = ?",aquario, product.getFull_name());

                movimentation.delete();

                int newSaldo = 0;

                List<Movimentation> movimentations = SugarRecord.find(Movimentation.class,"qrCode = ?", movimentation.getQr_code());
                for(int a=0; a< movimentations.size(); a++){

                    newSaldo += movimentations.get(a).getQtd();
                }


                if(saldos.size()>0){
                    Saldo saldo = saldos.get(0);
                    saldo.setQtd(newSaldo);
                    saldo.save();
                }


                movimentationList.remove(selected.keyAt(i));
                movimentationListAdapter.notifyDataSetChanged();//notify adapter

            }
        }
        //Toast.makeText(getApplicationContext(), selected.size() + " item deleted.", Toast.LENGTH_SHORT).show();//Show Toast
        Snackbar.make(getCurrentFocus(),"Movimentações removidas", Snackbar.LENGTH_LONG).show();
        mActionMode.finish();//Finish action mode after use

    }
}
