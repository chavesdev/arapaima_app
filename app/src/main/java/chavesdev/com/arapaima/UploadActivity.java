package chavesdev.com.arapaima;

import android.os.Handler;
import android.os.Looper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarRecord;

import java.util.List;

import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.Saldo;
import chavesdev.com.des.MovimentationDes;
import chavesdev.com.helpers.Connection;
import chavesdev.com.interfaces.MovimentationService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton btnUpload;
    private ImageView imgCheck;
    private DonutProgress progressUpload;
    private TextView txtClique;
    private int total, percent = 0, totalSended = 1;
    private List<Movimentation> movimentations;

    private Connection connection;
    private String urlInit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        connection = new Connection(this);
        if(!connection.hasUrl()){
            finish();
        }


        setContentView(R.layout.activity_upload);
        toolbar = (Toolbar) findViewById(R.id.toolbar_upload);
        toolbar.setTitle(getResources().getString(R.string.upload));
        //setSupportActionBar(toolbar);
        setActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        btnUpload = (ImageButton) findViewById(R.id.btn_start_upload);
        progressUpload = (DonutProgress) findViewById(R.id.progressUpload);
        txtClique = (TextView) findViewById(R.id.txtClique);
        imgCheck = (ImageView) findViewById(R.id.imgCheck);
    }

    public void startUpload(View view){

        btnUpload.setVisibility(View.INVISIBLE);
        progressUpload.setVisibility(View.VISIBLE);
        txtClique.setText(getResources().getString(R.string.txt_sendind));

        urlInit = connection.getUrlInit();
        urlInit+= getResources().getString(R.string.url_origin);

        movimentations = SugarRecord.listAll(Movimentation.class);

        Gson gsonMovimentation = new GsonBuilder().registerTypeAdapter(Movimentation.class, new MovimentationDes()).create();

        Retrofit retrofit_products = new Retrofit
                .Builder()
                .baseUrl(urlInit)
                .addConverterFactory(GsonConverterFactory.create(gsonMovimentation))
                .build();

        MovimentationService movimentationService = retrofit_products.create(MovimentationService.class);
        total = movimentations.size();

        for(int i=0;i<total;i++){
            Movimentation movimentation = movimentations.get(i);

            final Call<Movimentation> add = movimentationService.add(
                    movimentation.getQr_code(),
                    movimentation.getProduct_id(),
                    movimentation.getProvider_id(),
                    movimentation.getMovimentation_type_id(),
                    movimentation.getQtd(),
                    movimentation.getTotal(),
                    movimentation.getProduct_status_id(),
                    movimentation.getRecipient_id(),
                    movimentation.getAquario_id(),
                    movimentation.getArea_id(),
                    movimentation.getCreated()
            );

            final Handler handler = new Handler(Looper.getMainLooper());

            add.enqueue(new Callback<Movimentation>() {
                @Override
                public void onResponse(Call<Movimentation> call, Response<Movimentation> response) {
                    if(response.body()!=null){

                        handler.post(new ProgressUpdater(totalSended,total));

                        totalSended++;

                        finished();
                    }
                    else{
                        Log.i("Houve um erro:",response.message());
                    }

                }

                @Override
                public void onFailure(Call<Movimentation> call, Throwable t) {
                    Log.i("causa",t.getCause()+"");
                    Log.i("erro",t.getMessage()+ t.fillInStackTrace());
                    Toast.makeText(getBaseContext(),"Houve um problema na comunicação, tente novamente",Toast.LENGTH_LONG).show();
                }
            });
        }


    }

    public void finished(){
        SugarRecord.deleteAll(Movimentation.class);
        SugarRecord.deleteAll(Saldo.class);

        txtClique.setText("Concluído!");
        progressUpload.setVisibility(View.GONE);
        imgCheck.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 4000);
    }

    private class ProgressUpdater implements Runnable {
        private int mUploaded;
        private int mTotal;
        public ProgressUpdater(int uploaded, int total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {
            int percent = (mUploaded*100)/mTotal;
            progressUpload.setProgress(percent);
        }
    }
}
