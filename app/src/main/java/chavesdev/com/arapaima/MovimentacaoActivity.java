package chavesdev.com.arapaima;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.orm.SugarRecord;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.MovimentationType;
import chavesdev.com.classes.Product;
import chavesdev.com.classes.ProductStatus;
import chavesdev.com.classes.Provider;
import chavesdev.com.classes.Saldo;

public class MovimentacaoActivity extends AppCompatActivity {
    private static final String TAG = "Movimentacao";
    private String code;
    private Spinner spinner_movimentacao, spinnerProductStatus, spinner_fornecedores;
    Movimentation movimentation;

    TextView txt_area, txt_aquario;
    EditText edt_qtdSaida;
    AutoCompleteTextView autoTextEspecie;

    Product productSelected;
    Provider providerSelected;
    MovimentationType movimentationTypeSelected;
    ProductStatus productStatusSelected;
    ArrayList<MovimentationType> movimentationTypeList = new ArrayList<>();
    ArrayList<ProductStatus> productStatusList = new ArrayList<>();
    ArrayList<Provider> providerList = new ArrayList<>();
    ArrayList<Product> productsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentacao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            code = bundle.getString("code");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinner_movimentacao = (Spinner) findViewById(R.id.spn_movimentacao);
        populateMovimentationTypes();

        spinnerProductStatus = (Spinner) findViewById(R.id.spn_statusPeixe);
        populateProductStatus();

        spinner_fornecedores = (Spinner) findViewById(R.id.spn_fornecedor);
        populateProviders();


        autoTextEspecie = (AutoCompleteTextView) findViewById(R.id.autoTextEspecie);
        populateProducts();

        txt_aquario = (TextView) findViewById(R.id.txt_aquario);
        txt_area = (TextView) findViewById(R.id.txt_area);

        edt_qtdSaida = (EditText) findViewById(R.id.edt_qtd_saida);

        populaMovimentacao();

        spinner_movimentacao.requestFocus();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("activiy","on pause");
        Bundle bundle = new Bundle();
        bundle.putString("code",code);
    }

    private void populateMovimentationTypes(){
        movimentationTypeList = (ArrayList<MovimentationType>) SugarRecord.listAll(MovimentationType.class);
        if(movimentationTypeList!=null){
            ArrayAdapter<MovimentationType> adapterMovimentationType = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, movimentationTypeList);
            adapterMovimentationType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_movimentacao.setAdapter(adapterMovimentationType);
          }
    }

    private void populateProductStatus(){
        productStatusList = (ArrayList<ProductStatus>) SugarRecord.listAll(ProductStatus.class);
        if(productStatusList != null){
            ArrayAdapter<ProductStatus> adapterProductStatus = new ArrayAdapter<ProductStatus>(this,android.R.layout.simple_spinner_item, productStatusList);
            adapterProductStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerProductStatus.setAdapter(adapterProductStatus);
        }
    }

    private void populateProviders(){
        providerList = (ArrayList<Provider>) SugarRecord.listAll(Provider.class);
        if(providerList != null) {
            ArrayAdapter<Provider> adapterProviders = new ArrayAdapter<Provider>(this,
                    android.R.layout.simple_spinner_item, providerList);
            adapterProviders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_fornecedores.setAdapter(adapterProviders);
        }
    }

    private void populateProducts(){
        productsList = (ArrayList<Product>) SugarRecord.listAll(Product.class);
        ArrayAdapter<Product> adapter =
                new ArrayAdapter<Product>(this, android.R.layout.simple_list_item_1, productsList);
        autoTextEspecie.setAdapter(adapter);

        autoTextEspecie.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productSelected = (Product) parent.getItemAtPosition(position);
            }
        });
    }

    private void populaMovimentacao() {
        movimentation = new Movimentation();
        movimentation.setQr_code(code);

        txt_area.setText(String.valueOf(movimentation.getArea_id()));
        txt_aquario.setText(String.valueOf(movimentation.getAquario_id()));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
                if(movimentacaoIsValid()){
                    saveMovimentacao();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private boolean movimentacaoIsValid(){
        boolean is_valid = false;
        int cont = 6;//total must be 6;
        boolean advertised = false;
        String msg = "Nenhum campo pode estar em branco";

        if(productSelected == null){
            cont--;
            if(!advertised){
                msg = "Escolha um peixe da lista";
                autoTextEspecie.requestFocus();
                advertised = true;
            }
        }

        if(spinner_fornecedores.getSelectedItem().toString().isEmpty()){
            cont--;
            if(!advertised){
                spinner_fornecedores.requestFocus();
                advertised = true;
            }
        }

        if(spinner_movimentacao.getSelectedItem().toString().isEmpty()){
            cont--;
            if(!advertised){
                spinner_movimentacao.requestFocus();
                advertised = true;
            }
        }

        if(spinnerProductStatus.getSelectedItem().toString().isEmpty()){
            cont--;
            if(!advertised){
                spinnerProductStatus.requestFocus();
                advertised = true;
            }
        }

        if(edt_qtdSaida.getText().toString().isEmpty()){
            cont--;
            if(!advertised){
                edt_qtdSaida.requestFocus();
                advertised = true;
            }
        }

        if(cont==6){
            is_valid = true;
        }

        if(advertised){
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
            Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        }

        return is_valid;
    }

    private void saveMovimentacao() {
        final Calendar current_date = Calendar.getInstance();

        String datetime = current_date.get(Calendar.DAY_OF_MONTH)+"/"+
                (current_date.get(Calendar.MONTH)+1)+"/"+
                current_date.get(Calendar.YEAR) + " "+
                current_date.get(Calendar.HOUR_OF_DAY) + ":"+
                current_date.get(Calendar.MINUTE);


        movimentation.setProduct_id(productSelected.getServerId());

        providerSelected = (Provider) spinner_fornecedores.getSelectedItem();
        movimentation.setProvider_id(providerSelected.getServerId());

        movimentationTypeSelected = (MovimentationType) spinner_movimentacao.getSelectedItem();
        movimentation.setMovimentation_type_id(movimentationTypeSelected.getServerId());

        productStatusSelected = (ProductStatus) spinnerProductStatus.getSelectedItem();
        movimentation.setProduct_status_id(productStatusSelected.getServerId());

        movimentation.setQtd(Integer.parseInt(edt_qtdSaida.getText().toString()));
        movimentation.setTotal(Integer.parseInt(edt_qtdSaida.getText().toString()));
        movimentation.setCreated(datetime);
        movimentation.setModified(datetime);

        int status = spinnerProductStatus.getSelectedItemPosition();
        switch (status){
            case 0:
                movimentation.setEstoque("01.001");
                break;

            case 1:
                movimentation.setEstoque("01.002");
                break;

            default:
                movimentation.setEstoque("");
                break;
        }


        long moviment = movimentation.save();

        if(moviment>0){
            String aquario = movimentation.getAquariumString();
            List<Saldo> saldos = Saldo.find(Saldo.class,"aquario = ? and especie = ?",aquario, productSelected.getFull_name());

            int value_moviment = movimentation.getTotal();

            //if(type_pos>1){
            if(movimentationTypeSelected.getAction().equals("subtrai")){
                value_moviment *= (-1);//if qtd is negative
                movimentation.setQtd(value_moviment);
                movimentation.save();
            }

            if(saldos==null || saldos.size()==0){
                Log.i("Saldo","novo");

                Saldo saldo = new Saldo();

                saldo.setEspecie(productSelected.getFull_name());
                saldo.setAquario(aquario);
                saldo.setQtd(value_moviment);
                saldo.setQr(code);
                saldo.setCodProd(productSelected.getProd_cod());
                saldo.save();
            }
            else{
                Saldo saldo = saldos.get(0);
                int newQtd = saldo.getQtd()+value_moviment;
                saldo.setQtd(newQtd);
                saldo.save();
            }
            Toast.makeText(this,"Movimentação salva",Toast.LENGTH_LONG).show();
            finish();
        }
        else{
            Toast.makeText(this,"Erro ao salvar",Toast.LENGTH_LONG).show();
        }

    }

}
