package chavesdev.com.arapaima;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

import chavesdev.com.classes.Product;
import chavesdev.com.fragments.ExportaFragment;
import chavesdev.com.fragments.InventarioFragment;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, EasyPermissions.PermissionCallbacks{
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private int[] tabIcons = {
            R.drawable.inventario_nao_selecionado,
            R.drawable.movimentar_nao_selecionado,
            R.drawable.exportar_nao_selecionado
    };


    private int[] tabIconsSelected = {
        R.drawable.inventario_selecionado,
        R.drawable.movimentar_selecionado,
        R.drawable.exportar_selecionado
    };

    Toolbar toolbar_home;

    private static final String TAG = "ExportaFragment";
    private static final int RC_SMS_PERM = 122;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar_home = (Toolbar) findViewById(R.id.toolbar_home);
        toolbar_home.setTitle("Saldos");
        setSupportActionBar(toolbar_home);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar_home, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setBackgroundColor(getResources().getColor(R.color.white));
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setBackgroundColor(getResources().getColor(R.color.white));
        navigationView.setItemIconTintList(null);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tab);
        if (tabLayout != null) {

            tabLayout.addTab(tabLayout.newTab());
            tabLayout.addTab(tabLayout.newTab());
            tabLayout.addTab(tabLayout.newTab());
            Log.i("tab layout", "não é nulo");
        }

        setupTabIcons();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_upload:
                openUpload();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openUpload() {
        Intent intent_verify = new Intent(this,UploadActivity.class);
        startActivity(intent_verify);
    }


    private void openMovimentation(){
        Intent intent_camera = new Intent(HomeActivity.this,CaptureActivity.class);
        startActivity(intent_camera);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIconsSelected[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                tab.setIcon(tabIconsSelected[pos]);

                if(pos==1){
                    goCapture();
                }
                else if (pos==2){
                    changeviewPager(1,getResources().getString(R.string.title_exportacao));
                    verifyPermissions();
                }
                else{
                    changeviewPager(0,getResources().getString(R.string.title_inventario));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                tab.setIcon(tabIcons[pos]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void goCapture() {
        tabLayout.getTabAt(0).select();

        long prodsCount = SugarRecord.count(Product.class);

        if(prodsCount==0){
            Toast.makeText(this, "Você precisa importar dados do servidor",Toast.LENGTH_LONG).show();
        }
        else{
            Intent intent_camera = new Intent(HomeActivity.this,CaptureActivity.class);
            startActivity(intent_camera);
        }
    }

    public void changeviewPager(int pos, String title){
        viewPager.setCurrentItem(pos);
        toolbar_home.setTitle(title);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new InventarioFragment(), "Inventário");
        adapter.addFragment(new ExportaFragment(), "Exportar");

        viewPager.setAdapter(adapter);

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @AfterPermissionGranted(RC_SMS_PERM)
    private void verifyPermissions() {
        String[] perms = { Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE };
        if (!EasyPermissions.hasPermissions(this, perms)) {
            // Don't have permission, do the thing!
            EasyPermissions.requestPermissions(this,getString(R.string.rationale_storage),
                    RC_SMS_PERM, perms);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void closeDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_movimentations){
            Intent intent = new Intent(this,MovimentationsActivity.class);
            startActivity(intent);
            closeDrawer();
            return false;
        }

        if(id== R.id.nav_config){
            Intent intent = new Intent(this,ConfigActivity.class);
            startActivity(intent);
            closeDrawer();
            return false;
        }

        if(id == R.id.nav_update){
            Intent intent = new Intent(this,UpdateActivity.class);
            startActivity(intent);
            closeDrawer();
            return true;
        }

        return true;

    }

    class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if(position==0){
                return InventarioFragment.newInstance(null,null);
            }
            else{
                return mFragmentList.get(position);
            }
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Fragment fragment = ((ViewPagerAdapter)viewPager.getAdapter()).getItem(position);
            if (fragment != null) {
                fragment.onCreate(null);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Snackbar.make(findViewById(R.id.tab), "Essa permissão é necessária se você desejar realizar a exportação!",Snackbar.LENGTH_LONG).show();
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

    }
}