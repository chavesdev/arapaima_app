package chavesdev.com.arapaima;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ConfigActivity extends AppCompatActivity {

    String urlInit;
    EditText edtUrlInit;
    SharedPreferences sharedPref;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        toolbar.setTitle(getResources().getString(R.string.title_activity_config));
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        sharedPref = this.getSharedPreferences(getResources().getString(R.string.preference_name),Context.MODE_PRIVATE);
        urlInit = sharedPref.getString(getResources().getString(R.string.preference_name),"");

        edtUrlInit = (EditText) findViewById(R.id.edtUrlInit);

        edtUrlInit.setText(urlInit);
    }

    public void saveConfig(){
        urlInit = edtUrlInit.getText().toString();

        Log.e("url ",urlInit);

        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(getResources().getString(R.string.preference_name),urlInit);
        editor.commit();

        Toast.makeText(this, "Url alterada",Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
               saveConfig();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
