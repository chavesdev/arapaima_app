package chavesdev.com.arapaima;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.orm.SugarRecord;

import chavesdev.com.classes.Saldo;

public class UpdateInventarioActivity extends Activity {

    Saldo saldo;

    EditText edtSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_inventario);

        edtSaldo = (EditText) findViewById(R.id.edtSaldo);

        if(getIntent().getExtras()!=null){
            Bundle bundle = getIntent().getExtras();
            long saldo_id = bundle.getLong("saldo");

            saldo = SugarRecord.findById(Saldo.class,saldo_id);
            populaSaldo();
        }
    }

    public void populaSaldo(){
        edtSaldo.setText(String.valueOf(saldo.getQtd()));
    }

    public void saveSaldo(View view){
        int newQtd = Integer.parseInt(edtSaldo.getText().toString());
        saldo.setQtd(newQtd);
        saldo.save();
        finish();
    }
}
