package chavesdev.com.arapaima;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import chavesdev.com.classes.MovimentationType;
import chavesdev.com.classes.Product;
import chavesdev.com.classes.ProductStatus;
import chavesdev.com.classes.Provider;
import chavesdev.com.des.MovimentationTypeDes;
import chavesdev.com.des.ProductDes;
import chavesdev.com.des.ProductStatusDes;
import chavesdev.com.des.ProviderDes;
import chavesdev.com.helpers.Connection;
import chavesdev.com.interfaces.MovimentationTypeService;
import chavesdev.com.interfaces.ProductService;
import chavesdev.com.interfaces.ProductStatusService;
import chavesdev.com.interfaces.ProviderService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton btnStartUpdate;
    ProgressBar pgrBarUpdate;
    TextView txtUpdate;
    String urlInit, baseUrl;
    List<Product> listProducts = new ArrayList<>();
    List<Provider> listProviders = new ArrayList<>();
    List<MovimentationType> listMovimentationTypes = new ArrayList<>();
    List<ProductStatus> listProductStatus = new ArrayList<>();

    static final String TXT_CLICK_TO_UPDATE = "Clique para atualizar as informações";
    Connection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        toolbar.setTitle(getResources().getString(R.string.update_info));
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        btnStartUpdate = (ImageButton) findViewById(R.id.btnStartUpdate);
        pgrBarUpdate = (ProgressBar) findViewById(R.id.pgrBarUpdate);
        txtUpdate = (TextView) findViewById(R.id.txtUpdate);

        connection = new Connection(this);
        if(!connection.hasUrl()){
            finish();
        }
        else{
            urlInit = connection.getUrlInit();
            baseUrl = getResources().getString(R.string.url_origin);
            urlInit+= baseUrl;
        }
    }

    public void startUpdate(View view){
        btnStartUpdate.setVisibility(View.GONE);
        pgrBarUpdate.setVisibility(View.VISIBLE);
        txtUpdate.setText("Iniciando...");

        loadProducts();
    }

    public void stopUpdate(){
        Toast.makeText(getApplicationContext(),getResources().getString(R.string.alert_update_connection), Toast.LENGTH_LONG).show();

        btnStartUpdate.setVisibility(View.VISIBLE);
        pgrBarUpdate.setVisibility(View.GONE);
        txtUpdate.setText(TXT_CLICK_TO_UPDATE);
    }

    public void loadProducts(){
        txtUpdate.setText(R.string.str_loading_products);

        Gson gsonProduct = new GsonBuilder().registerTypeAdapter(Product.class, new ProductDes()).create();

        Retrofit retrofit_products = new Retrofit
                .Builder()
                .baseUrl(urlInit)
                .addConverterFactory(GsonConverterFactory.create(gsonProduct))
                .build();

        ProductService productService = retrofit_products.create(ProductService.class);
        final Call<Product> callListProducts = productService.list();

        callListProducts.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if(response.body() != null){

                    listProducts = (List<Product>) response.body();

                    loadProviders();
                }
                else{
                    stopUpdate();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.i("t",t.getMessage());
                stopUpdate();
            }
        });
    }

    public void loadProviders(){

        txtUpdate.setText(R.string.str_loading_providers);

        Gson gsonProviders = new GsonBuilder().registerTypeAdapter(Provider.class, new ProviderDes()).create();

        Retrofit retrofitProviders = new Retrofit
                .Builder()
                .baseUrl(urlInit)
                .addConverterFactory(GsonConverterFactory.create(gsonProviders))
                .build();

        ProviderService providerService = retrofitProviders.create(ProviderService.class);
        final Call<Provider> callListProviders = providerService.list();
        callListProviders.enqueue(new Callback<Provider>() {
            @Override
            public void onResponse(Call<Provider> call, Response<Provider> response) {
                if(response.body() != null){

                    listProviders = (List<Provider>) response.body();

                    loadMovimentationTypes();
                }
            }

            @Override
            public void onFailure(Call<Provider> call, Throwable t) {
                Log.i("t",t.getMessage());
                stopUpdate();
            }
        });

    }

    public void loadMovimentationTypes(){
        txtUpdate.setText(R.string.str_loading_movimentation_types);

        Gson gsonMovimentationTypes = new GsonBuilder().registerTypeAdapter(MovimentationType.class, new MovimentationTypeDes()).create();
        Retrofit retrofitMovimentationTypes = new Retrofit
                .Builder()
                .baseUrl(urlInit)
                .addConverterFactory(GsonConverterFactory.create(gsonMovimentationTypes))
                .build();
        MovimentationTypeService movimentationTypeService = retrofitMovimentationTypes.create(MovimentationTypeService.class);
        final Call<MovimentationType> callListMovimentationTypes = movimentationTypeService.list();
        callListMovimentationTypes.enqueue(new Callback<MovimentationType>() {
            @Override
            public void onResponse(Call<MovimentationType> call, Response<MovimentationType> response) {
                if(response.body()!=null){

                    listMovimentationTypes = (List<MovimentationType>) response.body();

                    Log.i("list movimentationTypes"," success");

                    loadProductStatus();
                }
            }

            @Override
            public void onFailure(Call<MovimentationType> call, Throwable t) {
                Log.i("t",t.getMessage());
                stopUpdate();
            }
        });

    }

    public void loadProductStatus(){
        txtUpdate.setText(R.string.str_loading_product_status);

        Gson gsonProductStatus = new GsonBuilder().registerTypeAdapter(ProductStatus.class, new ProductStatusDes()).create();
        Retrofit retrofitProductStatus = new Retrofit
                .Builder()
                .baseUrl(urlInit)
                .addConverterFactory(GsonConverterFactory.create(gsonProductStatus))
                .build();

        ProductStatusService productStatusService = retrofitProductStatus.create(ProductStatusService.class);
        final Call<ProductStatus> callListProductStatus = productStatusService.list();
        callListProductStatus.enqueue(new Callback<ProductStatus>() {
            @Override
            public void onResponse(Call<ProductStatus> call, Response<ProductStatus> response) {
                if(response.body()!=null){

                    listProductStatus = (List<ProductStatus>) response.body();

                    saveServerData();
                }
            }

            @Override
            public void onFailure(Call<ProductStatus> call, Throwable t) {
                Log.i("t",t.getMessage());
                stopUpdate();
            }
        });
    }

    public void saveServerData(){
        //product
        Product.deleteAll(Product.class);
        for(int i=0;i<listProducts.size(); i++){
            Product product = listProducts.get(i);
            product.save();
        }

        //productStatus
        ProductStatus.deleteAll(ProductStatus.class);
        for(int i=0;i<listProductStatus.size(); i++){
            ProductStatus productStatus = listProductStatus.get(i);
            productStatus.save();
        }

        //providers
        Provider.deleteAll(Provider.class);
        for(int i=0;i<listProviders.size(); i++){
            Provider provider = listProviders.get(i);
            provider.save();
        }

        //movimentationTypes
        MovimentationType.deleteAll(MovimentationType.class);
        for(int i=0;i<listMovimentationTypes.size(); i++){
            MovimentationType movimentationType = listMovimentationTypes.get(i);
            movimentationType.save();
        }

        updateFinished();

    }

    public void updateFinished(){
        Toast.makeText(this, "Informações atualizadas!", Toast.LENGTH_LONG).show();
        finish();
    }

}
