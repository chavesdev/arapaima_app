package chavesdev.com.des;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Arrays;

import chavesdev.com.classes.Product;

/**
 * Created by rodrigo-santos on 24/03/17.
 */

public class ProductDes implements JsonDeserializer<Object> {

    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Log.i("json ",json.toString());
        JsonElement product = null;
        if(json.getAsJsonObject().get("success").getAsBoolean()){
            //On List
            if(json.getAsJsonObject().get("data").isJsonArray()){
                JsonArray response_json = json.getAsJsonObject().get("data").getAsJsonArray();

                return Arrays.asList(new Gson().fromJson(response_json, Product[].class));
            }

            //On one
            else if( json.getAsJsonObject().get("data") != null ){
                product = json.getAsJsonObject().get("data");
                product = product.getAsJsonObject().get("Product");

                return ( new Gson().fromJson( product, Product.class ));
            }

            else{
                return null;
            }
        }
        else{

            Log.i("msg return: ",json.getAsJsonObject().get("msg").getAsString());

            return null;
        }
    }
}
