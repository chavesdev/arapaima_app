package chavesdev.com.des;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.Arrays;

import chavesdev.com.classes.Movimentation;

/**
 * Created by rodrigo-santos on 10/04/17.
 */

public class MovimentationDes implements JsonDeserializer<Object> {

    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        //Log.i("json ",json.toString());
        JsonElement movimentation = null;
        if(json.getAsJsonObject().get("success").getAsBoolean()){
            //On List
            if(json.getAsJsonObject().get("data").isJsonArray()){
                JsonArray response_json = json.getAsJsonObject().get("data").getAsJsonArray();

                return Arrays.asList(new Gson().fromJson(response_json, Movimentation[].class));
            }

            //On one
            else if( json.getAsJsonObject().get("data") != null ){
                movimentation = json.getAsJsonObject().get("data");
                movimentation = movimentation.getAsJsonObject().get("Movimentation");

                return ( new Gson().fromJson( movimentation, Movimentation.class ));
            }

            else{
                return null;
            }
        }
        else{

            Log.i("msg_return: ",json.getAsJsonObject().get("msg").getAsString());

            return null;
        }
    }
}
