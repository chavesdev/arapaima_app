package chavesdev.com.des;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Arrays;

import chavesdev.com.classes.Product;
import chavesdev.com.classes.ProductStatus;

/**
 * Created by rodrigo-santos on 26/03/17.
 */

public class ProductStatusDes implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Log.i("json product status ",json.toString());
        JsonElement movimentationType = null;
        if(json.getAsJsonObject().get("success").getAsBoolean()){
            //On List
            if(json.getAsJsonObject().get("data").isJsonArray()){
                JsonArray response_json = json.getAsJsonObject().get("data").getAsJsonArray();

                return Arrays.asList(new Gson().fromJson(response_json, ProductStatus[].class));
            }

            //On one
            else if( json.getAsJsonObject().get("data") != null ){
                movimentationType = json.getAsJsonObject().get("data");
                movimentationType = movimentationType.getAsJsonObject().get("ProductStatus");

                return ( new Gson().fromJson( movimentationType, ProductStatus.class ));
            }

            else{
                return null;
            }
        }
        else{

            Log.i("msg return: ",json.getAsJsonObject().get("msg").getAsString());

            return null;
        }
    }
}
