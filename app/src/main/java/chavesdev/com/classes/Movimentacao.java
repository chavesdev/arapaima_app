package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by root on 16/10/16.
 */

public class Movimentacao extends SugarRecord {

    //entrada, saida venda, saida morte, transferencia
    String qr;
    String type;
    String datetime;
    String codigo;
    int especieId;
    String codProd;
    String especie;
    String fornecedorId;
    String fornecedor;
    String estoque;
    String statusPeixe;
    int qtd_saida;
    int recipient;
    int aquario;
    int area;
    int total_saida;

    public Movimentacao() {
    }

    public Movimentacao(String qr, String type, String datetime, String codigo, int especieId, String codProd, String especie, String fornecedorId, String fornecedor, String estoque, String statusPeixe, int qtd_saida, int recipient, int aquario, int area, int total_saida) {
        this.qr = qr;
        this.type = type;
        this.datetime = datetime;
        this.codigo = codigo;
        this.especieId = especieId;
        this.codProd = codProd;
        this.especie = especie;
        this.fornecedorId = fornecedorId;
        this.fornecedor = fornecedor;
        this.estoque = estoque;
        this.statusPeixe = statusPeixe;
        this.qtd_saida = qtd_saida;
        this.recipient = recipient;
        this.aquario = aquario;
        this.area = area;
        this.total_saida = total_saida;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public int getQtd_saida() {
        return qtd_saida;
    }

    public void setQtd_saida(int qtd_saida) {
        this.qtd_saida = qtd_saida;
    }

    public int getAquario() {
        return aquario;
    }

    public void setAquario(int aquario) {
        this.aquario = aquario;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getTotal_saida() {
        return total_saida;
    }

    public void setTotal_saida(int total_saida) {
        this.total_saida = total_saida;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public int getRecipient() {
        return recipient;
    }

    public void setRecipient(int recipient) {
        this.recipient = recipient;
    }

    public String getStatusPeixe() {
        return statusPeixe;
    }

    public void setStatusPeixe(String statusPeixe) {
        this.statusPeixe = statusPeixe;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getFornecedorId() {
        return fornecedorId;
    }

    public void setFornecedorId(String fornecedorId) {
        this.fornecedorId = fornecedorId;
    }

    public int getEspecieId() {
        return especieId;
    }

    public void setEspecieId(int especieId) {
        this.especieId = especieId;
    }

    public String getEstoque() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque = estoque;
    }

    public String getCodProd() {
        return codProd;
    }

    public void setCodProd(String codProd) {
        this.codProd = codProd;
    }
}
