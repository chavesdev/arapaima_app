package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by rodrigo-santos on 26/03/17.
 */

public class MovimentationType extends SugarRecord {

    int serverId; //id from server
    String name;
    String action;
    boolean only_admin;

    public MovimentationType() {
    }

    public MovimentationType(int serverId, String name, String action, boolean only_admin) {
        this.serverId = serverId;
        this.name = name;
        this.action = action;
        this.only_admin = only_admin;
    }


    public int getServerId() {
        return this.serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isOnly_admin() {
        return only_admin;
    }

    public void setOnly_admin(boolean only_admin) {
        this.only_admin = only_admin;
    }

    public String toString(){
        return this.name;
    }
}
