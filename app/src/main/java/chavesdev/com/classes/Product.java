package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by rodrigo-santos on 24/03/17.
 */

public class Product extends SugarRecord{

    int serverId;
    int prod_id;
    String prod_cod;
    String full_name;
    String specie_name;
    String nome_vulgar;
    int area_id;
    int aquarium_id;


    public Product() {
    }

    public Product(int serverId, int prod_id, String prod_cod, String full_name, String specie_name, String nome_vulgar, int area_id, int aquarium_id) {
        this.serverId = serverId;
        this.prod_id = prod_id;
        this.prod_cod = prod_cod;
        this.full_name = full_name;
        this.specie_name = specie_name;
        this.nome_vulgar = nome_vulgar;
        this.area_id = area_id;
        this.aquarium_id = aquarium_id;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getProd_id() {

        return prod_id;
    }

    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_cod() {
        return prod_cod;
    }

    public void setProd_cod(String prod_cod) {
        this.prod_cod = prod_cod;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getSpecie_name() {
        return specie_name;
    }

    public void setSpecie_name(String specie_name) {
        this.specie_name = specie_name;
    }

    public String getNome_vulgar() {
        return nome_vulgar;
    }

    public void setNome_vulgar(String nome_vulgar) {
        this.nome_vulgar = nome_vulgar;
    }

    public int getArea_id() {
        return area_id;
    }

    public void setArea_id(int area_id) {
        this.area_id = area_id;
    }

    public int getAquarium_id() {
        return aquarium_id;
    }

    public void setAquarium_id(int aquarium_id) {
        this.aquarium_id = aquarium_id;
    }

    public String toString(){
        return this.full_name;
    }
}
