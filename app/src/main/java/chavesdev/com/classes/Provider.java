package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by rodrigo-santos on 25/03/17.
 */

public class Provider extends SugarRecord {

    int serverId;
    String provider_id;
    String name;

    public Provider() {
    }

    public Provider(int serverId, String provider_id, String name) {
        this.serverId = serverId;
        this.provider_id = provider_id;
        this.name = name;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }
}
