package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by rodrigo-santos on 27/03/17.
 */

public class Movimentation extends SugarRecord {

    String qr_code;
    int product_id;
    int provider_id;
    int movimentation_type_id;
    int qtd;
    int total;
    int product_status_id;
    int recipient_id;
    int aquarium_id;
    int area_id;
    String created, modified;

    String estoque; // extra value

    public Movimentation() {
    }

    public Movimentation(String qr_code, int product_id, int provider_id, int movimentation_type_id, int qtd, int total, int product_status_id, int recipient_id, int aquario_id, int area_id, String created, String modified) {
        this.qr_code = qr_code;
        this.product_id = product_id;
        this.provider_id = provider_id;
        this.movimentation_type_id = movimentation_type_id;
        this.qtd = qtd;
        this.total = total;
        this.product_status_id = product_status_id;
        this.recipient_id = recipient_id;
        this.aquarium_id = aquario_id;
        this.area_id = area_id;
        this.created = created;
        this.modified = modified;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;

        String[] parts = qr_code.split("\\.");
        this.setArea_id(Integer.parseInt(parts[1]));
        this.setAquario_id(Integer.parseInt(parts[2]));
        this.setRecipient_id(Integer.parseInt(parts[0]));
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public int getMovimentation_type_id() {
        return movimentation_type_id;
    }

    public void setMovimentation_type_id(int movimentation_type_id) {
        this.movimentation_type_id = movimentation_type_id;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getProduct_status_id() {
        return product_status_id;
    }

    public void setProduct_status_id(int product_status_id) {
        this.product_status_id = product_status_id;
    }

    public int getRecipient_id() {
        return recipient_id;
    }

    public void setRecipient_id(int recipient_id) {
        this.recipient_id = recipient_id;
    }

    public int getAquario_id() {
        return aquarium_id;
    }

    public void setAquario_id(int aquario_id) {
        this.aquarium_id = aquario_id;
    }

    public int getArea_id() {
        return area_id;
    }

    public void setArea_id(int area_id) {
        this.area_id = area_id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getAquariumString(){
        return this.recipient_id + "." + this.area_id + "." + this.aquarium_id;
    }

    public String getEstoque() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque = estoque;
    }

    //Relation methods

    public Product findProduct(){
        return SugarRecord.find(Product.class,"server_Id = ?",Integer.toString(this.getProduct_id())).get(0);
    }

    public MovimentationType findMovimentationType(){
        return SugarRecord.find(
                MovimentationType.class,
                "server_Id = ?",
                Integer.toString(this.getMovimentation_type_id())
        ).get(0);
    }
}
