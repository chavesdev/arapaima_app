package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by rodrigo-santos on 26/03/17.
 */

public class ProductStatus extends SugarRecord {

    int serverId; //id from server
    String name;

    public ProductStatus() {
    }

    public ProductStatus(int serverId, String name) {
        this.serverId = serverId;
        this.name = name;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name;
    }
}
