package chavesdev.com.classes;

import com.orm.SugarRecord;

/**
 * Created by root on 20/10/16.
 */
public class Saldo extends SugarRecord{
    private long id;
    String aquario;
    String especie;
    int qtd;
    String qr;
    String codProd;

    public Saldo() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getAquario() {
        return aquario;
    }

    public void setAquario(String aquario) {
        this.aquario = aquario;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getCodProd() {
        return codProd;
    }

    public void setCodProd(String codProd) {
        this.codProd = codProd;
    }
}
