package chavesdev.com.listeners;

import android.content.Context;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.view.ActionMode;
import androidx.core.view.MenuItemCompat;

import java.util.ArrayList;
import java.util.List;

import chavesdev.com.arapaima.MovimentationsActivity;
import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.recyclerAdapters.MovimentationListAdapter;

/**
 * Created by rodrigosantos on 13/04/17.
 */

public class Toolbar_ActionMode_Callback implements ActionMode.Callback {

    private Context context;
    private MovimentationListAdapter recyclerView_adapter;
//    private ListView_Adapter listView_adapter;
    private List<Movimentation> movimentations;
    private boolean isListViewFragment;


    public Toolbar_ActionMode_Callback(Context context, MovimentationListAdapter recyclerView_adapter, List<Movimentation> movimentations, boolean isListViewFragment) {
        this.context = context;
        this.recyclerView_adapter = recyclerView_adapter;
        //this.listView_adapter = listView_adapter;
        this.movimentations = movimentations;
        this.isListViewFragment = isListViewFragment;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_delete, menu);//Inflate the menu over action mode
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

        //Sometimes the meu will not be visible so for that we need to set their visibility manually in this method
        //So here show action menu according to SDK Levels
        if (Build.VERSION.SDK_INT < 11) {
            MenuItemCompat.setShowAsAction(menu.findItem(R.id.action_delete), MenuItemCompat.SHOW_AS_ACTION_NEVER);
        } else {
            menu.findItem(R.id.action_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//            menu.findItem(R.id.action_copy).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//            menu.findItem(R.id.action_forward).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                //(new MovimentationsActivity()).deleteRows();
                ((MovimentationsActivity) context).deleteRows();

                break;
        }
        return false;
    }


    @Override
    public void onDestroyActionMode(ActionMode mode) {

        //When action mode destroyed remove selected selections and set action mode to null
        //First check current fragment action mode
        recyclerView_adapter.removeSelection();  // remove selection
        new MovimentationsActivity().setNullToActionMode();
    }
}
