package chavesdev.com.recyclerAdapters;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.MovimentationType;
import chavesdev.com.classes.Product;
import chavesdev.com.recyclerAdapters.holders.MovimentationViewHolder;

/**
 * Created by rodrigo-santos on 11/04/17.
 */

public class MovimentationListAdapter extends RecyclerView.Adapter<MovimentationViewHolder> {
    List<Movimentation> movimentationList;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;

    public MovimentationListAdapter() {
    }

    public MovimentationListAdapter(List<Movimentation> movimentationList, Context context) {
        this.movimentationList = movimentationList;
        this.context = context;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    public void setMovimentationList(List<Movimentation> movimentationList) {
        this.movimentationList = movimentationList;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public MovimentationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.movimentation_item_holder,null);
        return new MovimentationViewHolder(v,context);
    }

    @Override
    public void onBindViewHolder(MovimentationViewHolder holder, int position) {
        Movimentation movimentation = movimentationList.get(position);
        MovimentationType movimentationType = movimentation.findMovimentationType();
        Product product = movimentation.findProduct();
        holder.getTxtEspecieTitle().setText(product.getFull_name());
        holder.getTxtMovimentationType().setText(movimentationType.getName());
        holder.getTxtQtd().setText(String.valueOf(movimentation.getQtd()));

        holder.setMovimentation(movimentation);
        holder.setMovimentationType(movimentationType);

        /** Change background color of the selected items in list view  **/
        holder.itemView
                .setBackgroundColor(mSelectedItemsIds.get(position) ? 0x99e6e6e6
                        : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return (movimentationList==null)? 0 : movimentationList.size();
    }

    /***
     * Methods required for do selections, remove selections, etc.
     */

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
