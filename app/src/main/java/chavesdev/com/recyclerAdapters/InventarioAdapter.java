package chavesdev.com.recyclerAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Saldo;
import chavesdev.com.recyclerAdapters.holders.InventarioViewHolder;


public class InventarioAdapter extends RecyclerView.Adapter<InventarioViewHolder> {
    List<Saldo> saldoList;
    Context context;
    private int lastPosition = -1;

    public InventarioAdapter() {
    }

    public InventarioAdapter(List<Saldo> saldoList, Context context) {
        this.saldoList = saldoList;
        this.context = context;
    }

    public void setSaldoList(List<Saldo> saldoList) {
        this.saldoList = saldoList;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public InventarioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.inventario_item_view,null);
        return new InventarioViewHolder(v,context);
    }

    @Override
    public void onBindViewHolder(InventarioViewHolder holder, int position) {
        Saldo saldo = saldoList.get(position);
        holder.getTxtAquario().setText(String.valueOf(saldo.getAquario()));
        holder.getTxtEspecieTitle().setText(saldo.getEspecie());
        //holder.getTxtEspecieTitle().setSelected(true);
        holder.getTxtQtd().setText(String.valueOf(saldo.getQtd()));

        holder.setSaldo(saldo);

        setAnimation(holder.getContainer(), position);
    }

    @Override
    public int getItemCount() {
     if(saldoList==null){
         return 0;
     }
     else{
         return saldoList.size();
     }
    }


    private void setAnimation(View view, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            view.startAnimation(animation);
            lastPosition = position;
        }
    }

}
