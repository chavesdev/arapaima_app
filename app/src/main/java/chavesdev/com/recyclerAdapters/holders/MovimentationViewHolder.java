package chavesdev.com.recyclerAdapters.holders;

import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.SugarRecord;

import java.util.List;

import chavesdev.com.arapaima.MovimentationsActivity;
import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.MovimentationType;
import chavesdev.com.classes.Product;
import chavesdev.com.classes.Saldo;

/**
 * Created by rodrigo-santos on 11/04/17.
 */

public class MovimentationViewHolder extends RecyclerView.ViewHolder {
    TextView txtEspecieTitle, txtMovimentationType, txtQtd;
    Context context;
    Movimentation movimentation;
    MovimentationType movimentationType;
    RelativeLayout movimentationContainer;

    public MovimentationViewHolder(View itemView, Context context) {
        super(itemView);

        this.context = context;
        txtEspecieTitle = (TextView) itemView.findViewById(R.id.txtEspecieTitle);
        txtMovimentationType = (TextView) itemView.findViewById(R.id.txtMovimentationType);
        txtQtd = (TextView) itemView.findViewById(R.id.txtQtd);
        movimentationContainer = (RelativeLayout) itemView.findViewById(R.id.movimentationContainer);


        itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    private void showDialog(){
        View view = LayoutInflater.from(context).inflate(R.layout.alert_dialog_edittext,null);
        final EditText input = (EditText) view.findViewById(R.id.edtUpdateSaldo);

        input.setText(String.valueOf(this.movimentation.getQtd()));
        input.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);//Cria o gerador do AlertDialog
        builder.setTitle("Atualizar valor Saldo");//define o titulo
        builder.setView(view);

        builder.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                int newQtd = Integer.parseInt(input.getText().toString());
                if(movimentation.getQtd() < 0){
                    newQtd *= -1;
                }

                getTxtQtd().setText(String.valueOf(newQtd));

                updateMovimentation(newQtd,movimentation);
            }
        });

        AlertDialog alert = builder.create();

        alert.show();
        input.requestFocus();
    }

    private void updateMovimentation(int newQtd, Movimentation movimentation) {
        Product product = SugarRecord.find(Product.class,"server_Id = ?",String.valueOf(movimentation.getProduct_id())).get(0);
        List<Saldo> saldos = Saldo.find(Saldo.class,"aquario = ? and especie = ?",movimentation.getAquariumString(), product.getFull_name());
        Saldo saldo = null;
        int newSaldo = 0;
        movimentation.setQtd(newQtd);
        movimentation.save();

        List<Movimentation> movimentations = SugarRecord.find(Movimentation.class,"qrCode = ?", movimentation.getQr_code());
        for(int i=0; i< movimentations.size(); i++){

            newSaldo += movimentations.get(i).getQtd();
        }


        if(saldos.size() > 0){
            saldo = saldos.get(0);
            saldo.setQtd(newSaldo);
        }
        else{
            saldo = new Saldo();
            saldo.setEspecie(product.getSpecie_name());
            saldo.setAquario(movimentation.getAquariumString());
            saldo.setQtd(newQtd);
            saldo.setQr(movimentation.getQr_code());
            saldo.setCodProd(product.getProd_cod());
        }

        saldo.save();


    }

    public TextView getTxtEspecieTitle() {
        return txtEspecieTitle;
    }

    public TextView getTxtMovimentationType() {
        return txtMovimentationType;
    }

    public TextView getTxtQtd() {
        return txtQtd;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Movimentation getMovimentation() {
        return movimentation;
    }

    public void setMovimentation(Movimentation movimentation) {
        this.movimentation = movimentation;
    }

    public MovimentationType getMovimentationType() {
        return movimentationType;
    }

    public void setMovimentationType(MovimentationType movimentationType) {
        this.movimentationType = movimentationType;
    }
}
