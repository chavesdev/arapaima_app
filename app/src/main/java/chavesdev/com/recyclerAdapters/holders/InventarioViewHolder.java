package chavesdev.com.recyclerAdapters.holders;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Saldo;

/**
 * Created by root on 20/10/16.
 */

public class InventarioViewHolder extends RecyclerView.ViewHolder {
    TextView txtEspecieTitle, txtAquario, txtQtd;
    Context context;
    RelativeLayout container;
    Saldo saldo;

    public InventarioViewHolder(View itemView, final Context context) {
        super(itemView);

        this.context = context;

        txtEspecieTitle = (TextView) itemView.findViewById(R.id.txtEspecieTitle);
        txtAquario = (TextView) itemView.findViewById(R.id.txtIdAquario);
        txtQtd = (TextView) itemView.findViewById(R.id.txtQtd);
        container = (RelativeLayout) itemView.findViewById(R.id.itemContainer);

    }

    public void setSaldo(Saldo saldo) {
        this.saldo = saldo;
    }

    public TextView getTxtEspecieTitle() {
        return txtEspecieTitle;
    }

    public TextView getTxtAquario() {
        return txtAquario;
    }

    public TextView getTxtQtd() {
        return txtQtd;
    }

    public RelativeLayout getContainer() {
        return container;
    }
}
