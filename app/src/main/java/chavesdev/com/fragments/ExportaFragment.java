package chavesdev.com.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.orm.SugarRecord;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Movimentation;
import chavesdev.com.classes.MovimentationType;
import chavesdev.com.classes.Product;
import chavesdev.com.classes.ProductStatus;
import chavesdev.com.classes.Provider;
import chavesdev.com.classes.Saldo;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExportaFragment extends Fragment {
    private ImageButton btn_exporta;
    private DonutProgress progressExport;
    private TextView txtAperte;
    private int total, percent = 0, total_inserted = 1;
    List<Movimentation> movimentacaoList;
    List<Saldo> saldoList;
    File directory;
    String date_file;

    File file_movimentacoes, file_inventario;

    public ExportaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExportaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExportaFragment newInstance(String param1, String param2) {
        ExportaFragment fragment = new ExportaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view_exporta = inflater.inflate(R.layout.fragment_exporta, container, false);
        progressExport = (DonutProgress) view_exporta.findViewById(R.id.progressExport);
        txtAperte = (TextView) view_exporta.findViewById(R.id.txtAperte);

        btn_exporta = (ImageButton) view_exporta.findViewById(R.id.btn_start_expot);
        btn_exporta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportFiles();
            }
        });

        return view_exporta;
    }


    public void exportFiles(){

        btn_exporta.setVisibility(View.INVISIBLE);
        txtAperte.setText(getResources().getString(R.string.str_gerando_arquivo_movimentacao));
        progressExport.setVisibility(View.VISIBLE);

        //movimentacaoList = SugarRecord.listAll(Movimentacao.class);
        movimentacaoList = SugarRecord.listAll(Movimentation.class);
        saldoList = SugarRecord.listAll(Saldo.class);
        total = movimentacaoList.size() + saldoList.size();

        File sdCard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        //File sdCard = getContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        directory = new File(sdCard+"/arapaima");

        Log.i("absolute", directory.getAbsolutePath());
        //create directory if not exist
        if(!directory.isDirectory()){
            directory.mkdir();
        }

        final Calendar current_date = Calendar.getInstance();

        date_file = current_date.get(Calendar.DAY_OF_MONTH)+"_"+
                (current_date.get(Calendar.MONTH)+1)+"_"+
                current_date.get(Calendar.YEAR);

        exportaMovimentacoes();
    }

    public void exportaMovimentacoes(){

        String file_name_movimentacoes = "arapaima_movimentacoes_"+date_file+".xls";

        file_movimentacoes = new File(directory,file_name_movimentacoes);

        Log.i("file",file_movimentacoes.getAbsolutePath());
        WritableWorkbook wb = null;
        try {
            wb = Workbook.createWorkbook(file_movimentacoes);
            wb.createSheet("layout", 0);
            WritableSheet sheet = wb.getSheet(0);

//            sheet.addCell(new Label(0,0,"IDPRD"));
//            sheet.addCell(new Label(1,0,"IDFOR"));
//            sheet.addCell(new Label(2,0,"ESTOQUE"));
//            sheet.addCell(new Label(3,0,"QUANTIDADE"));
//            sheet.addCell(new Label(4,0,"PREÇO"));
//            sheet.addCell(new Label(5,0,"DATA"));
//            sheet.addCell(new Label(6,0,"CODIGOPRD"));
//            sheet.addCell(new Label(7,0,"NOMEFANTASIA"));
//            sheet.addCell(new Label(8,0,"STATUS"));
//            sheet.addCell(new Label(9,0,"FORNECEDOR"));
//            sheet.addCell(new Label(10,0,"QRCODE"));
//            sheet.addCell(new Label(11,0,"MOVIMENTAÇÂO"));

            int sheet_pos = 1;
            for (int i=0;i<movimentacaoList.size();i++){
                Movimentation movimentacao = movimentacaoList.get(i);
                Provider provider = SugarRecord.find(Provider.class,"server_Id = ?",Integer.toString(movimentacao.getProvider_id())).get(0);
                Product product = SugarRecord.find(Product.class,"server_Id = ?",Integer.toString(movimentacao.getProduct_id())).get(0);
                ProductStatus productStatus = SugarRecord.find(ProductStatus.class,"server_Id = ? ",Integer.toString(movimentacao.getProduct_status_id())).get(0);
                MovimentationType movimentationType = SugarRecord.find(MovimentationType.class,"server_Id = ?",Integer.toString(movimentacao.getMovimentation_type_id())).get(0);

                sheet.addCell(new Label(0,sheet_pos,String.valueOf(product.getProd_id())));
                sheet.addCell(new Label(1,sheet_pos,String.format("%06d", Integer.parseInt(provider.getProvider_id()))));
                sheet.addCell(new Label(2,sheet_pos,movimentacao.getEstoque()));
                sheet.addCell(new Label(3,sheet_pos,String.valueOf(movimentacao.getQtd())));
                sheet.addCell(new Label(4,sheet_pos,""));
                sheet.addCell(new Label(5,sheet_pos,movimentacao.getCreated()));
                sheet.addCell(new Label(6,sheet_pos,product.getProd_cod()));
                sheet.addCell(new Label(7,sheet_pos,product.getFull_name()));
                sheet.addCell(new Label(8,sheet_pos,productStatus.getName()));
                sheet.addCell(new Label(9,sheet_pos,provider.getName()));
                sheet.addCell(new Label(10,sheet_pos,movimentacao.getQr_code()));
                sheet.addCell(new Label(11,sheet_pos,movimentationType.getName()));


                percent = (total_inserted*100)/total;
                progressExport.setProgress(percent);

                sheet_pos++;
                total_inserted++;
            }


            wb.write();
            wb.close();

            exportaInventario();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    public void exportaInventario(){

        String file_name_inventario = "arapaima_inventario_"+date_file+".xls";

        file_inventario = new File(directory,file_name_inventario);

        Log.i("file",file_inventario.getAbsolutePath());
        WritableWorkbook wb = null;
        try {
            wb = Workbook.createWorkbook(file_inventario);
            wb.createSheet("layout", 0);
            WritableSheet sheet = wb.getSheet(0);

//            sheet.addCell(new Label(0,0,"CODPROD"));
//            sheet.addCell(new Label(1,0,"NOMEFANTASIA"));
//            sheet.addCell(new Label(2,0,"QUANTIDADE"));
//            sheet.addCell(new Label(3,0,"QRCODE"));

            int sheet_pos = 1;
            for (int i=0;i<saldoList.size();i++){
                Saldo saldo = saldoList.get(i);
                sheet.addCell(new Label(0,sheet_pos,saldo.getCodProd()));
                sheet.addCell(new Label(1,sheet_pos,saldo.getEspecie()));
                sheet.addCell(new Label(2,sheet_pos,String.valueOf(saldo.getQtd())));
                sheet.addCell(new Label(3,sheet_pos,saldo.getQr()));

                percent = (total_inserted*100)/total;
                progressExport.setProgress(percent);

                sheet_pos++;
                total_inserted++;
            }


            wb.write();
            wb.close();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    txtAperte.setText("CLIQUE PARA VER OS ARQUIVOS");
                    progressExport.setVisibility(View.GONE);
                    btn_exporta.setVisibility(View.VISIBLE);

                    btn_exporta.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //openFile(file_inventario);
                            openFolder();
                        }
                    });
                }
            }, 1500);

            removeAll();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    public void openFolder() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri uri = Uri.fromFile(directory);
        intent.setDataAndType(uri, "application/vnd.ms-excel");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getContext().startActivity(Intent.createChooser(intent, "Listar arquivos"));
    }

    public void removeAll(){
        SugarRecord.deleteAll(Saldo.class);
        SugarRecord.deleteAll(Movimentation.class);
    }
}
