package chavesdev.com.fragments;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.SugarRecord;
import java.util.ArrayList;
import java.util.List;
import chavesdev.com.arapaima.R;
import chavesdev.com.classes.Saldo;
import chavesdev.com.recyclerAdapters.InventarioAdapter;
import chavesdev.com.uicontrollers.DividerItemDecoration;


public class InventarioFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView recyclerViewInventario;
    TextView txtInventario;
    ConstraintLayout placeholder;


    private List<Saldo> saldoList = new ArrayList<>();
    InventarioAdapter inventarioAdapter ;

    public InventarioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InventarioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InventarioFragment newInstance(String param1, String param2) {
        InventarioFragment fragment = new InventarioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //loadSaldos();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inventario, container, false);

        //txtInventario = view.findViewById(R.id.txtInventario);

        recyclerViewInventario = view.findViewById(R.id.recyclerInventario);

        placeholder = view.findViewById(R.id.placeholder);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerViewInventario.addItemDecoration(itemDecoration);

        recyclerViewInventario.setItemAnimator(new DefaultItemAnimator());
        recyclerViewInventario.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewInventario.setLayoutManager(mLayoutManager);

        inventarioAdapter = new InventarioAdapter(saldoList,getContext());
        recyclerViewInventario.setAdapter(inventarioAdapter);

        loadSaldos();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume(){
        super.onResume();
        loadSaldos();
    }

    public void loadSaldos(){

        saldoList = SugarRecord.listAll(Saldo.class);
        Log.i("qtd inventatio",String.valueOf(saldoList.size()));

        if(saldoList.size()>0){
            placeholder.setVisibility(View.GONE);
        }
        inventarioAdapter.setSaldoList(saldoList);
        inventarioAdapter.notifyDataSetChanged();
    }

}
